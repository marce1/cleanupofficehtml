﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using HtmlAgilityPack;

namespace CleanupOfficeHtml.BL
{
    public class CleanupEngine
    {
        #region Fields

        private HtmlDocument _document;

        #endregion

        #region Properties
        private List<HtmlNode> NodesToRemove { get; set; }
        #endregion

        #region Constructors

        public CleanupEngine(string filePath)
        {
            NodesToRemove = new List<HtmlNode>();
            _document = new HtmlDocument();
            _document.Load(filePath);
        }

        public CleanupEngine(HtmlDocument document)
        {
            NodesToRemove = new List<HtmlNode>();
            _document = document;
        }

        #endregion

        #region Methods

        public void Cleanup(string[] removeElements, HtmlDocument document = null)
        {
            if (document != null)
                _document = document;

            var htmlNode = _document.DocumentNode.SelectSingleNode("html");
            CleanupHeader();
            CleanupAllAttributes(htmlNode);
            MarkElementsToRemove(htmlNode, removeElements);
            CleanupElements();
            CleanupComments();
        }

        private void CleanupComments()
        {
            foreach (var node in _document.DocumentNode.SelectNodes("//comment()"))
            {
                var nodeInnerHtml = node.InnerHtml;
                node.Remove();
                ConsoleLogInfo(string.Format("Comment {0} was removed", nodeInnerHtml));
            }
        }

        public void SaveResult(string path)
        {
            try
            {
                using (FileStream fileStream = new FileStream(path, FileMode.CreateNew, FileAccess.ReadWrite))
                {
                    _document.Save(fileStream, Encoding.UTF8);
                }
                ConsoleLogInfo("Result saved");
            }
            catch (Exception ex)
            {
                ConsoleLogError(ex.Message);
            }
        }

        private void CleanupElements()
        {
            foreach (HtmlNode nodeToRemove in NodesToRemove)
            {
                string name = nodeToRemove.Name;
                nodeToRemove.Remove();
                ConsoleLogInfo(string.Format("Element <{0}> removed", name));
            }
        }
        private void MarkElementsToRemove(HtmlNode node, string[] elementsToRemove)
        {
            foreach (var childNode in node.ChildNodes)
            {
                if (string.IsNullOrEmpty(childNode.InnerText.Replace("&nbsp;", " ").Replace("\r", " ").Replace("\n", " ").Trim()))
                    NodesToRemove.Add(childNode);

                foreach (var elementToRemove in elementsToRemove)
                {
                    if (childNode.Name == elementToRemove)
                        NodesToRemove.Add(childNode);
                }

                if (childNode.HasChildNodes)
                    MarkElementsToRemove(childNode, elementsToRemove);
            }
        }
        private void CleanupHeader()
        {
            var htmlNode = _document.DocumentNode.SelectSingleNode("html");
            var title = htmlNode.SelectSingleNode("head/title");
            htmlNode.SelectSingleNode("head").RemoveAllChildren();  // remove all head childs
            htmlNode.SelectSingleNode("head").AppendChild(title);   // add title back to head element

            ConsoleLogInfo("Header cleaned");
        }
        private void CleanupAllAttributes(HtmlNode node)
        {
            CleanupAttributes(node);

            foreach (var childNode in node.ChildNodes)
            {
                if (childNode.Attributes.Count > 0)
                {
                    CleanupAttributes(childNode);
                }
                if (childNode.HasChildNodes)
                    CleanupAllAttributes(childNode);
            }

            ConsoleLogInfo(string.Format("Attributes for element <{0}> cleaned", node.Name));
        }
        private static void CleanupAttributes(HtmlNode node)
        {
            HtmlAttribute hrefAttribute = null;
            if (node.Attributes.Contains("href"))
                hrefAttribute = node.Attributes["href"];

            node.Attributes.RemoveAll();    // remove all attributes

            if (hrefAttribute != null)
                node.Attributes.Append(hrefAttribute);  // put back href attribute
        }

        private void ConsoleLogInfo(string text)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(text);
            Console.ResetColor();
        }
        private void ConsoleLogError(string text)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine(text);
            Console.ResetColor();
        }

        public override string ToString()
        {
            return _document.DocumentNode.OuterHtml;
        }

        #endregion
    }
}
