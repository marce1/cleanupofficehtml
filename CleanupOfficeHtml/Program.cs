﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleanupOfficeHtml.BL;
using static System.Console;

namespace CleanupOfficeHtml
{
    class Program
    {
        static void Main(string[] args)
        {
            string path;
            if (args.Length == 0)
            {
                WriteLine("Enter path to file");
                path = ReadLine();
            }
            else
            {
                path = args[0];
            }
            CleanupEngine engine = new CleanupEngine(path);
            engine.Cleanup(new []{"o:p"});

            WriteLine("Press Enter key to show result");
            ReadLine();

            WriteLine("--------------");
            WriteLine(engine.ToString());
            WriteLine("--------------");

            WriteLine("Save result? Y/N");
            var saveResult = ReadLine();
            if (!string.IsNullOrEmpty(saveResult) && saveResult.ToUpper().Trim() == "Y")
            {
                var newFilePath = Path.Combine(Path.GetDirectoryName(path), "file.html");
                engine.SaveResult(newFilePath);
            }

            WriteLine("Press Enter key to close this application.");
            ReadLine();
        }
    }
}
